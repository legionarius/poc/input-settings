#
# Makefile
# bogdan, 2021-02-12 22:14
#

all:
	@echo "Makefile needs your attention"

native:
	scons platform=linux

.PHONY: godot-cpp
godot-cpp:
	cd godot-cpp; scons platform=linux generate_bindings=yes bits=64 use_custom_api_file=yes custom_api_file=../api.json -j4

gen-json-api:
	godot --gdnative-generate-json-api api.json

# vim:ft=make
#
