#ifndef INPUTITEM_H
#define INPUTITEM_H

#include <Button.hpp>
#include <Godot.hpp>
#include <HBoxContainer.hpp>
#include <InputEvent.hpp>
#include <InputEventKey.hpp>
#include <Label.hpp>
#include <OS.hpp>

namespace godot {
class InputItem : public HBoxContainer {
	GODOT_CLASS(InputItem, HBoxContainer);

private:
	bool wait_for_input;
	int keycode;
	String name;
	void _on_button_pressed();

public:
	static void _register_methods();
	void _init();
	void _ready();
	void _input(const Ref<InputEvent> event);
	int get_keycode();
	void set_keycode(int code);
	String get_name();
	void set_name(String name);
};
} // namespace godot

#endif // INPUTITEM_H
