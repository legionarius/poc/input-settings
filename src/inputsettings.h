#ifndef INPUTSETTINGS_H
#define INPUTSETTINGS_H

#include "inputitem.h"
#include <Control.hpp>
#include <File.hpp>
#include <Godot.hpp>
#include <JSON.hpp>
#include <JSONParseResult.hpp>
#include <PackedScene.hpp>
#include <Resource.hpp>
#include <ResourceLoader.hpp>
#include <SceneTree.hpp>
#include <VBoxContainer.hpp>
#include <vector>

namespace godot {

class InputSettings : public Control {
	GODOT_CLASS(InputSettings, Control);

private:
	VBoxContainer *container;
	std::vector<std::pair<String, int>> inputs;

public:
	static void _register_methods();
	void read_inputs();
	void _init();
	void _ready();
	InputSettings();
	~InputSettings();
};
} // namespace godot

#endif // INPUTSETTINGS_H
