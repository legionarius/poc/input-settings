/*
 * Copyright 2021 Bogdan Cordier <bogdan.cordier@hadaly.fr>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "inputsettings.hpp"

using namespace godot;

InputSettings::InputSettings() {}

InputSettings::~InputSettings() {}

void InputSettings::read_inputs() {
	Ref<File> file = File::_new();
	Error json_file = file->open("inputs.json", File::ModeFlags::READ);
	if (json_file == Error::ERR_FILE_CANT_OPEN) {
		Godot::print("Can't open file !");
	} else if (json_file == Error::ERR_FILE_CANT_READ) {
		Godot::print("Can't read file !");
	} else if (json_file == Error::ERR_FILE_NOT_FOUND) {
		Godot::print("File not found !");
	}
	Dictionary json_results =
			JSON::get_singleton()->parse(file->get_as_text())->get_result();
	Array n = json_results["inputs"];
	String json_string = JSON::get_singleton()->print(json_results);
	//     Godot::print("name: " + (const String &)json_results["inputs"]);
	for (int i = 0; i < n.size(); i++) {
		Dictionary input = n[i];
		std::pair<String, int> item =
				std::pair<String, int>(input["name"], input["keycode"]);
		inputs.push_back(item);
	}
}

void InputSettings::_ready() {
	container = Object::cast_to<VBoxContainer>(get_node("InputItems"));
	Ref<PackedScene> inputItem = ResourceLoader::get_singleton()->load(
			"entity/InputSettings/InputItem.tscn");
	for (auto item : inputs) {
		InputItem *instance = Object::cast_to<InputItem>(inputItem->instance());
		instance->set_name(item.first);
		instance->set_keycode(item.second);
		container->add_child(instance);
	}
}

void InputSettings::_init() {
	read_inputs();
}

void InputSettings::_register_methods() {
	register_method("_init", &InputSettings::_init);
	register_method("_ready", &InputSettings::_ready);
}
