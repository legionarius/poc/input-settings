#include "inputitem.h"

using namespace godot;

void InputItem::_init() {
	wait_for_input = false;
}

void InputItem::_ready() {
	Button *button = Object::cast_to<Button>(get_node("Button"));
	button->connect("pressed", this, "_on_button_pressed");
}

void InputItem::_input(const Ref<InputEvent> event) {
	Ref<InputEventKey> event_key = event;
	if (wait_for_input && event_key.is_valid() && event_key->is_pressed()) {
		// TODO : use scancode->string for button text

		// TODO: wait for correct input ( != 0 )
		keycode = event_key->get_scancode();
		if (keycode != 0) {
			String key_name = OS::get_singleton()->get_scancode_string(keycode);
			Button *button = Object::cast_to<Button>(get_node("Button"));
			button->set_text(key_name);
			wait_for_input = false;
			set_process_input(true);
		}
	}
}

void InputItem::_on_button_pressed() {
	Godot::print("Pressed");
	Button *button = Object::cast_to<Button>(get_node("Button"));
	button->set_text("Waiting for input...");
	wait_for_input = true;
	set_focus_mode(Control::FOCUS_NONE);
}

void InputItem::set_keycode(int code) {
	Button *button = Object::cast_to<Button>(get_node("Button"));
	button->set_text(std::to_string(code).c_str());
}

void InputItem::set_name(String name) {
	Label *label = Object::cast_to<Label>(get_node("Label"));
	label->set_text(name);
	this->name = name;
}

int InputItem::get_keycode() {
	Button *button = Object::cast_to<Button>(get_node("Button"));
	return button->get_text().to_int();
}

String InputItem::get_name() {
	Label *label = Object::cast_to<Label>(get_node("Label"));
	return label->get_text();
}

void InputItem::_register_methods() {
	register_method("_ready", &InputItem::_ready);
	register_method("_init", &InputItem::_init);
	register_method("_input", &InputItem::_input);
	register_method("set_name", &InputItem::set_name);
	register_method("get_name", &InputItem::get_name);
	register_method("_on_button_pressed", &InputItem::_on_button_pressed);
}
